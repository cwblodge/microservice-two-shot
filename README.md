# Wardrobify

Team:

* Cosimo - Hats
* Gustaaavvooo - Shoes

## Design

CSS - Hats:

In a typical CSS setup, global stylesheets were used to define universal style rules applicable across the entire website. These styles included settings for body backgrounds, default text sizes, and foundational layout components. This approach ensured a consistent appearance and behavior throughout the site.

For specific sections or components of a webpage, component-specific stylesheets were employed. Each part of the website, such as navigation bars, footers, or forms, had its own dedicated stylesheet. This modular approach allowed developers to manage styles more efficiently. By compartmentalizing the CSS, it became easier to debug and update individual sections without affecting the overall styling. This structure not only kept the CSS organized but also facilitated collaboration among developers by clearly defining responsibilities for different parts of the site.


CSS - Hats:

In a typical CSS setup, global stylesheets were used to define universal style rules applicable across the entire website. These styles included settings for body backgrounds, default text sizes, and foundational layout components. This approach ensured a consistent appearance and behavior throughout the site.

For specific sections or components of a webpage, component-specific stylesheets were employed. Each part of the website, such as navigation bars, footers, or forms, had its own dedicated stylesheet. This modular approach allowed developers to manage styles more efficiently. By compartmentalizing the CSS, it became easier to debug and update individual sections without affecting the overall styling. This structure not only kept the CSS organized but also facilitated collaboration among developers by clearly defining responsibilities for different parts of the site.


## Shoes microservice

In the Wardrobify project, the Shoes microservice played a crucial role in seamlessly managing shoe collections within the broader context of the wardrobe. Leveraging Django's powerful model system, I structured the Shoe model to encompass essential details such as material, style, color, and storage bin within the wardrobe, facilitated by the BinVO model. This structured approach ensured efficient organization and retrieval of shoe data, tightly integrated with the Wardrobe microservice.

To enable smooth interaction with the Shoes microservice, I meticulously crafted RESTful API handlers. These handlers empowered users with a spectrum of actions, ranging from viewing detailed shoe information to dynamically adding or removing shoes from their collections. By establishing these endpoints, I facilitated seamless communication between the React front-end and the backend, enhancing the user experience.

On the React front-end, I meticulously designed and implemented components tailored to meet users' needs regarding shoe management. These components provided intuitive interfaces for users to browse through their shoe collections, effortlessly add new shoes, and declutter by removing unwanted ones. By seamlessly linking these components with the corresponding API endpoints, I ensured a cohesive user experience, enabling users to navigate their shoe collections with ease and efficiency.

In essence, by harmonizing Django models, RESTful APIs, and React components, I crafted a user-friendly ecosystem within Wardrobify for efficiently managing shoes. This integration not only facilitated seamless interaction with shoe data but also enriched the broader wardrobe context, enhancing the overall user experience within the application.


## Hats microservice

In the Wardrobify project, the Hats microservice needed to work smoothly with the Wardrobe microservice. The Hat model in Django gave me a structured way to keep track of hat details like fabric, style, color, and even where it was stored in the wardrobe, thanks to the LocationVO model. This integration ensured that I could organize and access hat data effectively within the broader wardrobe context.

To make all this happen, I created RESTful API handlers for the Hats microservice. These handlers let users do everything from viewing hat details to adding or deleting hats. With these endpoints in place, my React components easily communicated with the backend, giving users a seamless experience as they managed their hat collections.

In the React front-end, I built components that let users see their hats, add new ones, and delete ones they no longer needed. By linking these components to our API endpoints, I made sure users could navigate through their hat collections effortlessly. Overall, by blending our Django models, RESTful APIs, and React components, I created a user-friendly environment for organizing hats within Wardrobify.
