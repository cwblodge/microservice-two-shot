from django.urls import path
from .views import hat_list, hat_detail

urlpatterns = [
    path("hats/", hat_list, name="list_hats"),
    path("hats/<int:location_vo_id>/", hat_detail, name="hat_detail"),
]
