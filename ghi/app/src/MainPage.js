import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import './styles/mainPage.css';
function HatColumn(props) {
  return (
    <div className="col">
      {props.list.map(hat => (
        <div key={hat.id} className="card mb-3 shadow">
          <img src={hat.picture_url} className="card-img-top" alt={`Image of ${hat.style_name}`} />
          <div className="card-body">
            <h5 className="card-title">{hat.style_name}</h5>
            <h6 className="card-subtitle mb-2 text-muted">Fabric: {hat.fabric}, Color: {hat.color}</h6>
          </div>
        </div>
      ))}
    </div>
  );
}
function ShoeColumn(props) {
  return (
    <div className="col">
      {props.list.map(shoe => (
        <div key={shoe.id} className="card mb-3 shadow">
          <img src={shoe.picture_url} className="card-img-top" alt={`Image of ${shoe.model_name}`} />
          <div className="card-body">
            <h5 className="card-title">{shoe.manufacturer} - {shoe.model_name}</h5>
            <h6 className="card-subtitle mb-2 text-muted">{shoe.color}</h6>
            <p className="card-text">{shoe.description}</p>
          </div>
        </div>
      ))}
    </div>
  );
}
function MainPage() {
  const [hatColumns, setHatColumns] = useState([[], [], []]);
  const [shoeColumns, setShoeColumns] = useState([[], [], []]);
  useEffect(() => {
    const fetchItems = async (url, setColumns) => {
      try {
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          const columns = [[], [], []];
          let i = 0;
          for (const item of data.hats || data.shoes) {
            columns[i].push(item);
            i = (i + 1) % 3;
          }
          setColumns(columns);
        } else {
          throw new Error('Failed to fetch items');
        }
      } catch (error) {
        console.error('Error fetching items:', error);
      }
    };
    fetchItems('http://localhost:8090/api/hats/', setHatColumns);
    fetchItems('http://localhost:8080/api/shoes/', setShoeColumns);
  }, []);
  return (
    <>
      <div className="px-4 py-5 my-5 text-center bg-info">
        <h1 className="display-5 fw-bold">WARDROBIFY</h1>
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
          <Link to="/add-hat" className="btn btn-primary btn-lg px-4 gap-3">Add a New Hat</Link>
          <Link to="/shoes/new" className="btn btn-secondary btn-lg px-4">Add New Shoes</Link>
        </div>
      </div>
      <div className="container">
        <h2>Our Collections</h2>
        <div className="row">
          <h3>Hat Collection</h3>
          {hatColumns.map((hatList, index) => <HatColumn key={index} list={hatList} />)}
          <h3>Shoe Closet</h3>
          {shoeColumns.map((shoeList, index) => <ShoeColumn key={index} list={shoeList} />)}
        </div>
      </div>
    </>
  );
}
export default MainPage;
