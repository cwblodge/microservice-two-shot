import React, { useState, useEffect } from 'react';

function HatList() {
  const [hats, setHats] = useState([]);

  useEffect(() => {
    fetchHats();
  }, []);

  const fetchHats = async () => {
    try {
      const response = await fetch('http://localhost:8090/api/hats/');
      if (response.ok) {
        const data = await response.json();
        setHats(data.hats);  
      } else {
        console.error('Failed to fetch data');
      }
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  const deleteHat = async (id) => {
    try {
      const response = await fetch(`http://localhost:8090/api/hats/${id}`, {
        method: 'DELETE'
      });
      if (response.ok) {
        setHats(prevHats => prevHats.filter(hat => hat.id !== id));
      } else {
        console.error('Failed to delete the hat');
      }
    } catch (error) {
      console.error('Error deleting the hat:', error);
    }
  };

  return (
    <div className="container mt-4">
      <h2>Hat List</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Style Name</th>
            <th>Fabric</th>
            <th>Color</th>
            <th>Picture</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {hats.map(hat => (
            <tr key={hat.id}>
              <td>{hat.style_name}</td>
              <td>{hat.fabric}</td>
              <td>{hat.color}</td>
              <td><img src={hat.picture_url} alt={`Hat styled as ${hat.style_name}`} style={{width: '100px', height: 'auto'}}/></td>
              <td>
                <button className="btn btn-danger" onClick={() => deleteHat(hat.id)}>Delete</button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default HatList;

