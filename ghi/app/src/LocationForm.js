import React, { useEffect, useState } from 'react';

function LocationForm() {
    const [closetName, setClosetName] = useState('');
    const [sectionNumber, setSectionNumber] = useState('');
    const [shelfNumber, setShelfNumber] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {
            closet_name: closetName,
            section_number: sectionNumber,
            shelf_number: shelfNumber,
        };

        const locationUrl = 'http://localhost:8100/api/locations/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        try {
            const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
                const newLocation = await response.json();
                console.log(newLocation);
                setClosetName('');
                setSectionNumber('');
                setShelfNumber('');
            } else {
                throw new Error('Failed to create location');
            }
        } catch (error) {
            console.error('POST error:', error);
        }
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create New Location</h1>
                    <form onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input onChange={(e) => setClosetName(e.target.value)} value={closetName} placeholder="Closet Name" required type="text" id="closetName" className="form-control"/>
                            <label htmlFor="closetName">Closet Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={(e) => setSectionNumber(e.target.value)} value={sectionNumber} placeholder="Section Number" required type="number" id="sectionNumber" className="form-control"/>
                            <label htmlFor="sectionNumber">Section Number</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={(e) => setShelfNumber(e.target.value)} value={shelfNumber} placeholder="Shelf Number" required type="number" id="shelfNumber" className="form-control"/>
                            <label htmlFor="shelfNumber">Shelf Number</label>
                        </div>
                        <button type="submit" className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default LocationForm;
