// BinForm.js
import React, { useEffect, useState } from 'react';

function BinForm() {
    const [closetName, setClosetName] = useState('');
    const [binNumber, setBinNumber] = useState('');
    const [binSize, setBinSize] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {
            closet_name: closetName,
            bin_number: binNumber,
            bin_size: binSize,
        };
        const binUrl = 'http://localhost:8100/api/bins/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        try {
            const response = await fetch(binUrl, fetchConfig);
            if (response.ok) {
                const newBin = await response.json();
                console.log(newBin);
                setClosetName('');
                setBinNumber('');
                setBinSize('');
            } else {
                throw new Error('Failed to create bin');
            }
        } catch (error) {
            console.error('POST error:', error);
        }
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create New Bin</h1>
                    <form onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input onChange={(e) => setClosetName(e.target.value)} value={closetName} placeholder="Closet Name" required type="text" id="closetName" className="form-control"/>
                            <label htmlFor="closetName">Closet Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={(e) => setBinNumber(e.target.value)} value={binNumber} placeholder="Bin Number" required type="number" id="binNumber" className="form-control"/>
                            <label htmlFor="binNumber">Bin Number</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={(e) => setBinSize(e.target.value)} value={binSize} placeholder="Bin Size" required type="number" id="binSize" className="form-control"/>
                            <label htmlFor="binSize">Bin Size</label>
                        </div>
                        <button type="submit" className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default BinForm;
