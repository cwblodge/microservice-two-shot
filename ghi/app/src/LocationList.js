import React, { useState, useEffect } from 'react';

function LocationList() {
  const [locations, setLocations] = useState([]);

  useEffect(() => {
    fetchLocations();
  }, []);

  const fetchLocations = async () => {
    try {
      const response = await fetch('http://localhost:8100/api/locations/');
      if (response.ok) {
        const data = await response.json();
        setLocations(data.locations);  
      } else {
        console.error('Failed to fetch data');
      }
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  const deleteLocation = async (id) => {
    try {
      const response = await fetch(`http://localhost:8100/api/locations/${id}`, {
        method: 'DELETE'
      });
      if (response.ok) {
        setLocations(prevLocations => prevLocations.filter(location => location.id !== id));
      } else {
        console.error('Failed to delete the location');
      }
    } catch (error) {
      console.error('Error deleting the location:', error);
    }
  };

  return (
    <div className="container mt-4">
      <h2>Location List</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Closet Name</th>
            <th>Section Number</th>
            <th>Shelf Number</th>
            <th>Delete</th> 
          </tr>
        </thead>
        <tbody>
          {locations.map(location => (
            <tr key={location.id}>
              <td>{location.closet_name}</td>
              <td>{location.section_number}</td>
              <td>{location.shelf_number}</td>
              <td>
                <button className="btn btn-danger" onClick={() => deleteLocation(location.id)}>
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default LocationList;

