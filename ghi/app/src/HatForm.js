import React, { useState, useEffect } from 'react';


function CreateHat() {
    const [hat, setHat] = useState({
        fabric: '',
        style_name: '',
        color: '',
        picture_url: '',
        location: ''
    });
    const [locations, setLocations] = useState([]);

    useEffect(() => {
        const fetchLocations = async () => {
            try {
                const response = await fetch('http://localhost:8100/api/locations/');
                if (response.ok) {
                    const data = await response.json();
                    setLocations(data.locations); 
                } else {
                    console.error('Failed to fetch locations:', response.status);
                }
            } catch (error) {
                console.error('Error fetching locations:', error);
            }
        };
        fetchLocations();
    }, []);

    const handleChange = (event) => {
        const { name, value } = event.target;
        setHat(prevHat => ({
            ...prevHat,
            [name]: value
        }));
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        try {
            const response = await fetch('http://localhost:8090/api/hats/', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(hat)
            });
            if (!response.ok) {
                throw new Error(`Failed to create hat: ${response.status}`);
            }
            const data = await response.json();
            console.log('Hat created:', data);
            alert('Hat created!');
            setHat({ fabric: '', style_name: '', color: '', picture_url: '', location: '' });
        } catch (error) {
            console.error('Error creating hat:', error.message);
        }
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create New Hat</h1>
                    <form onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input type="text" name="fabric" className="form-control" id="fabric" value={hat.fabric} onChange={handleChange} placeholder="Fabric"/>
                            <label htmlFor="fabric">Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input type="text" name="style_name" className="form-control" id="style_name" value={hat.style_name} onChange={handleChange} placeholder="Style Name"/>
                            <label htmlFor="style_name">Style Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input type="text" name="color" className="form-control" id="color" value={hat.color} onChange={handleChange} placeholder="Color"/>
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input type="url" name="picture_url" className="form-control" id="picture_url" value={hat.picture_url} onChange={handleChange} placeholder="Picture URL"/>
                            <label htmlFor="picture_url">Picture URL</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select name="location" className="form-control" id="location" value={hat.location} onChange={handleChange}>
                                <option value="">Select a Location</option>
                                {locations.map(location => (
                                    <option key={location.id} value={location.id}>
                                        {location.closet_name} - Section {location.section_number} - Shelf {location.shelf_number}
                                    </option>
                                ))}
                            </select>
                            <label htmlFor="location">Location</label>
                        </div>
                        <button type="submit" className="btn btn-primary">Create Hat</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default CreateHat;


