import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Nav from './Nav';
import MainPage from './MainPage';
import HatForm from './HatForm';
import HatList from './HatList';
import LocationForm from './LocationForm';
import LocationList from './LocationList';
import './styles/global.css';
import ShoeList from './ShoeList';
import ShoeForm from './ShoeForm';
import BinList from './BinList';
import BinForm from './BinForm';
function App() {
  return (
    <Router>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats" element={<HatList />} />
          <Route path="/create-hat" element={<HatForm />} />
          <Route path="/locations" element={<LocationList />} />
          <Route path="/create-location" element={<LocationForm />} />
          <Route path="/shoes" element={<ShoeList />} />
          <Route path="/create-shoe" element={<ShoeForm />} />
          <Route path="/bins" element={<BinList />} />
          <Route path="/create-bin" element={<BinForm />} />
        </Routes>
      </div>
    </Router>
  );
}
export default App;





