import React, { useState, useEffect } from 'react';

function ShoeList() {
  const [shoes, setShoes] = useState([]);

  useEffect(() => {
    const fetchShoes = async () => {
      try {
        const response = await fetch('http://localhost:8080/api/shoes/');
        if (response.ok) {
          const data = await response.json();
          setShoes(data.shoes);
        } else {
          console.error('Failed to fetch data');
        }
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchShoes();
  }, []);

  const deleteShoe = async (id) => {
    try {
      const response = await fetch(`http://localhost:8080/api/shoes/${id}`, {
        method: 'DELETE'
      });
      if (response.ok) {
        // Si la eliminación es exitosa, actualizamos el estado de los zapatos excluyendo el zapato eliminado
        setShoes(prevShoes => prevShoes.filter(shoe => shoe.id !== id));
      } else {
        console.error('Failed to delete the shoe');
      }
    } catch (error) {
      console.error('Error deleting the shoe:', error);
    }
  };

  return (
    <div className="container mt-4">
      <h2>Shoe List</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>Model Name</th>
            <th>Image</th>
            <th>Color</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {shoes.map(shoe => (
            <tr key={shoe.id}>
              <td>{shoe.manufacturer}</td>
              <td>{shoe.model_name}</td>
              <td><img alt="" src={shoe.url} /></td>
              <td>{shoe.color}</td>
              <td>
                <button className="btn btn-danger" onClick={() => deleteShoe(shoe.id)}>
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ShoeList;
