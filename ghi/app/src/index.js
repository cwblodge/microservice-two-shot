
import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));

async function loadHats() {
    try {
        const response = await fetch('http://localhost:8090/api/hats/');
        if (response.ok) {
            const data = await response.json();
            root.render(
                <React.StrictMode>
                    <App hats={data.hats} />  
                </React.StrictMode>
            );
        } else {
            throw new Error('Failed to fetch hats: ' + response.statusText);
        }
    } catch (error) {
        console.error('Error loading hats:', error);
    }
}

loadHats();
