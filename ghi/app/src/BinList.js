// BinList.js
import React, { useState, useEffect } from 'react';

function BinList() {
    const [bins, setBins] = useState([]);

    useEffect(() => {
        fetchBins();
    }, []);

    const fetchBins = async () => {
        try {
            const response = await fetch('http://localhost:8100/api/bins/');
            if (response.ok) {
                const data = await response.json();
                setBins(data.bins);
            } else {
                console.error('Failed to fetch data');
            }
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    const deleteBin = async (id) => {
        try {
            const response = await fetch(`http://localhost:8100/api/bins/${id}`, {
                method: 'DELETE'
            });
            if (response.ok) {
                setBins(prevBins => prevBins.filter(bin => bin.id !== id));
            } else {
                console.error('Failed to delete the bin');
            }
        } catch (error) {
            console.error('Error deleting the bin:', error);
        }
    };

    return (
        <div className="container mt-4">
            <h2>Bin List</h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Closet Name</th>
                        <th>Bin Number</th>
                        <th>Bin Size</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {bins.map(bin => (
                        <tr key={bin.id}>
                            <td>{bin.closet_name}</td>
                            <td>{bin.bin_number}</td>
                            <td>{bin.bin_size}</td>
                            <td>
                                <button className="btn btn-danger" onClick={() => deleteBin(bin.id)}>
                                    Delete
                                </button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default BinList;
