import React, { useState, useEffect } from 'react';

function CreateShoe() {
    const [shoe, setShoe] = useState({
        manufacturer: '',
        model_name: '',
        color: '',
        url: '',
        bin: ''
    });
    const [bins, setBins] = useState([]);
    const [error, setError] = useState('');

    useEffect(() => {
        const fetchBins = async () => {
            try {
                const response = await fetch('http://localhost:8100/api/bins/');
                if (response.ok) {
                    const data = await response.json();
                    setBins(data.bins);
                } else {
                    throw new Error('Failed to fetch bins:' + response.status);
                }
            } catch (error) {
                console.error('Error fetching bins:', error);
            }
        };
        fetchBins();
    }, []);

    const handleChange = ({ target: { name, value } }) => {
        setShoe(prevShoe => ({
            ...prevShoe,
            [name]: value
        }));
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        try {
            // Validación básica
            if (!shoe.manufacturer || !shoe.model_name || !shoe.color || !shoe.url || !shoe.bin) {
                throw new Error('All fields are required');
            }

            const response = await fetch('http://localhost:8080/api/shoes/', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(shoe)
            });
            if (!response.ok) {
                throw new Error('Failed to create shoe');
            }
            const data = await response.json();
            console.log('Shoe created:', data);
            alert('Shoe created successfully!');
            // Reset form fields after successful submission
            setShoe({
                manufacturer: '',
                model_name: '',
                color: '',
                url: '',
                bin: ''
            });
            setError('');
        } catch (error) {
            console.error('Error creating shoe:', error.message);
            setError(error.message);
        }
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create New Shoe</h1>
                    <form onSubmit={handleSubmit}>
                        {error && <div className="alert alert-danger">{error}</div>}
                        <div className="form-floating mb-3">
                            <input type="text" name="manufacturer" className="form-control" id="manufacturer" value={shoe.manufacturer} onChange={handleChange} placeholder="Manufacturer" />
                            <label htmlFor="manufacturer">Manufacturer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input type="text" name="model_name" className="form-control" id="model_name" value={shoe.model_name} onChange={handleChange} placeholder="Model Name" />
                            <label htmlFor="model_name">Model Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input type="text" name="color" className="form-control" id="color" value={shoe.color} onChange={handleChange} placeholder="Color" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input type="url" name="url" className="form-control" id="url" value={shoe.url} onChange={handleChange} placeholder="Url/Link" />
                            <label htmlFor="url">URL</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input type="number" name="bin" className="form-control" id="bin" value={shoe.bin} onChange={handleChange} placeholder="Bin" />
                            <label htmlFor="bin">Bin</label>
                        </div>
                        <button type="submit" className="btn btn-primary">Create Shoe</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default CreateShoe;

