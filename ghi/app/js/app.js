document.addEventListener('DOMContentLoaded', async () => {
    // Initialize the form submission for creating a new shoe
    const form = document.getElementById('new-shoe-form');
    form.addEventListener('submit', async function (event) {
        event.preventDefault();
        const formData = {
            model_name: document.getElementById('model_name').value,
            manufacturer: document.getElementById('manufacturer').value,
            color: document.getElementById('color').value,
            url: document.getElementById('url').value,
            bin: parseInt(document.getElementById('bin').value, 10)
        };
        try {
            const response = await fetch('http://localhost:8080/api/shoes/', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(formData)
            });
            if (!response.ok) {
                throw new Error('Failed to create shoe');
            }
            const result = await response.json();
            console.log('Shoe created:', result);
            form.reset();
        } catch (error) {
            console.error('Error:', error.message);
        }
    });
    // Load bins for the form's select element
    try {
        const response = await fetch('http://localhost:8100/api/bins/');
        if (!response.ok) {
            throw new Error('Failed to fetch bins');
        }
        const bins = await response.json();
        const binSelect = document.getElementById('bin');
        bins.forEach(bin => {
            const option = new Option(bin.name, bin.id);
            binSelect.add(option);
        });
    } catch (error) {
        console.error('Error:', error.message);
    }
    // Load and display cards for hats
    try {
        const response = await fetch('http://localhost:8090/api/hats/');
        if (!response.ok) {
            throw new Error(`Failed to fetch hats. Server responded with ${response.status}: ${response.statusText}`);
        }
        const hats = await response.json();
        const cardRow = document.getElementById('card-row');
        let columnIndex = 0;
        const columns = [];
        hats.forEach(hat => {
            const html = createCard(hat.style_name, hat.fabric, hat.color, hat.picture_url, hat.location);
            if (!columns[columnIndex]) {
                columns[columnIndex] = document.createElement('div');
                columns[columnIndex].className = 'col-md-4';
                cardRow.appendChild(columns[columnIndex]);
            }
            columns[columnIndex].innerHTML += html;
            columnIndex = (columnIndex + 1) % 3;
        });
    } catch (error) {
        console.error(error);
        const cardRow = document.getElementById('card-row');
        cardRow.innerHTML = `
            <div class="alert alert-danger" role="alert">
                Error: ${error.message}
            </div>
        `;
    }
});
function createCard(styleName, fabric, color, pictureUrl, locationName) {
    return `
        <div class="card my-3 shadow-sm">
            <img src="${pictureUrl}" class="card-img-top" alt="Image of ${styleName}">
            <div class="card-body">
                <h5 class="card-title">${styleName}</h5>
                <h6 class="card-subtitle mb-2 text-muted">Fabric: ${fabric}, Color: ${color}</h6>
                <p class="card-text">Located at: ${locationName}</p>
            </div>
        </div>
    `;
}

