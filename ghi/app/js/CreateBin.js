document.addEventListener('DOMContentLoaded', async () => {
    const form = document.getElementById('new-hat-form');
    form.addEventListener('submit', async function (event) {
        event.preventDefault();
        const formData = {
            model_name: document.getElementById('model_name').value,
            manufacturer: document.getElementById('manufacturer').value,
            color: document.getElementById('color').value,
            url: document.getElementById('url').value,
            bin: parseInt(document.getElementById('bin').value, 10)
        };
        try {
            const response = await fetch('http://localhost:8080/api/shoes/', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(formData)
            });
            if (!response.ok) {
                throw new Error('Failed to create shoe');
            }
            const result = await response.json();
            console.log('Shoe created:', result);
            form.reset();
        } catch (error) {
            console.error('Error:', error.message);
        }
    });
    try {
        const response = await fetch('http://localhost:8080/api/bins/');
        if (!response.ok) {
            throw new Error('Failed to fetch bins');
        }
        const data = await response.json();
        const binSelect = document.getElementById('bin');
        data.forEach(bin => {
            const option = document.createElement('option');
            option.value = bin.id;
            option.textContent = bin.name;
            binSelect.appendChild(option);
        });
    } catch (error) {
        console.error('Error:', error.message);
    }
});
