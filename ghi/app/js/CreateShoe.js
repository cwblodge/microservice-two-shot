function createCard(modelName, manufacturer, color, Url, binName) {
    return `
        <div class="card my-3 shadow-sm">
            <img src="${Url}" class="card-img-top" alt="Image of ${modelName}">
            <div class="card-body">
                <h5 class="card-title">${modelName}</h5>
                <h6 class="card-subtitle mb-2 text-muted">Fabric: ${manufacturer}}, Color: ${color}</h6>
                <p class="card-text">Located at: ${binName}</p>
            </div>
        </div>
    `;
}
window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8080/api/shoes/';
    const cardRow = document.getElementById('card-row');
    let columnIndex = 0;
    const columns = [];
    try {
        const response = await fetch(url);
        if (!response.ok) {
            throw new Error(`Failed to fetch shoes. Server responded with ${response.status}: ${response.statusText}`);
        }
        const data = await response.json();
        data.forEach(hat => {
            const modelName = shoe.modelname;
            const manufacturer = shoe.manufacturer;
            const color = shoe.color;
            const Url = shoe.url;
            const binName = shoe.bin;  // Assuming 'location' is a string attribute
            const html = createCard(modelName, manufacturer, color, Url, binName)
            if (!columns[columnIndex]) {
                columns[columnIndex] = document.createElement('div');
                columns[columnIndex].className = 'col-md-4';
                cardRow.appendChild(columns[columnIndex]);
            }
            columns[columnIndex].innerHTML += html;
            columnIndex = (columnIndex + 1) % 3;
        });
    } catch (e) {
        console.error(e);
        cardRow.innerHTML = `
            <div class="alert alert-danger" role="alert">
                Error: ${e.message}
            </div>
        `;
    }
});
