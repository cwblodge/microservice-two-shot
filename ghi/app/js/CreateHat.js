document.addEventListener('DOMContentLoaded', async () => {
    const form = document.getElementById('new-hat-form');
    form.addEventListener('submit', async function (event) {
        event.preventDefault();

        const formData = {
            style_name: document.getElementById('style_name').value,
            fabric: document.getElementById('fabric').value,
            color: document.getElementById('color').value,
            picture_url: document.getElementById('picture_url').value,
            location: parseInt(document.getElementById('location').value, 10) 
        };

        try {
            const response = await fetch('http://localhost:8090/api/hats/', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(formData)
            });
            if (!response.ok) {
                throw new Error('Failed to create hat');
            }
            const result = await response.json();
            console.log('Hat created:', result);
            form.reset();
        } catch (error) {
            console.error('Error:', error.message);
        }
    });

    try {
        const response = await fetch('http://localhost:8100/api/locations/');
        if (!response.ok) {
            throw new Error('Failed to fetch locations');
        }
        const data = await response.json();
        const locationSelect = document.getElementById('location');
        data.forEach(location => {
            const option = new Option(location.name, location.id);
            locationSelect.add(option);
        });
    } catch (error) {
        console.error('Error:', error.message);
    }
});
