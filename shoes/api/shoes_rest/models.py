from django.db import models
from django.urls import reverse

class BinVO(models.Model):
    import_href = models.CharField(max_length=200)

    def __str__(self):
        return f"the path is {self.import_href}"

class Shoe(models.Model):
    manufacturer = models.CharField(max_length=200)
    model_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    url = models.URLField()
    bin = models.ForeignKey(BinVO, on_delete=models.CASCADE)
